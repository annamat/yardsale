var mongoose = require('mongoose');

module.exports = function(connection) {

    var Schema = mongoose.Schema;

    var thingSchema = new Schema({
        name: String,
        size: Number,
        owner: String,
        dateAdded: Date
    });

    var Thing = connection.model('Thing', thingSchema);

    return Thing;
}
