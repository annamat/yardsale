define(['angular'], function (angular) {
    'use strict';

    /* Directives */
    var myAppDirectives = angular.module('myAppDirectives', []);

    myAppDirectives.directive('appVersion', ['version', function (version) {
        return function (scope, elm, attrs) {
            elm.text(version);
        };
    }]);

    myAppDirectives.directive('thingRow', ThingRow);

    function ThingRow() {
        return {
            restrict: 'A',
            templateUrl: '/templates/thingRow.html',
            vm: {
                thing: "=thingRow"
            }
        };
    }

    return myAppDirectives;
});


